
package modelo;

import java.util.List;

/**
 *
 * @author chris
 */
public interface intermediaProfesorClase {
    
  public ClaseProfesor obtenerIdProfesor(String id_estudiante);
  public ClaseProfesor obtenerIdClase(String id_clase);
  public List<ClaseProfesor> obtenerIdClasesProfesores();
  public ClaseProfesor crearClaseProfesor(int id_profesor, int id_clase);
  public boolean eliminarIdClaseProfesor(int id_profesor);
}
