/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author chris
 */
public class ClaseProfesor {
   
    private int id_claseProfesor;
    private int fk_profesor;
    private int fk_clase;

    public ClaseProfesor(int id_estudiante, int id_clase) {
       
        this.fk_profesor = id_estudiante;
        this.fk_clase = id_clase;
    }

    /**
     * @return the id_claseProfesor
     */
    public int getId_claseProfesor() {
        return id_claseProfesor;
    }

    /**
     * @param id_claseProfesor the id_claseProfesor to set
     */
    public void setId_claseProfesor(int id_claseProfesor) {
        this.id_claseProfesor = id_claseProfesor;
    }

    /**
     * @return the fk_profesor
     */
    public int getFk_profesor() {
        return fk_profesor;
    }

    /**
     * @param fk_profesor the fk_profesor to set
     */
    public void setFk_profesor(int fk_profesor) {
        this.fk_profesor = fk_profesor;
    }

    /**
     * @return the fk_clase
     */
    public int getFk_clase() {
        return fk_clase;
    }

    /**
     * @param fk_clase the fk_clase to set
     */
    public void setFk_clase(int fk_clase) {
        this.fk_clase = fk_clase;
    }

    @Override
    public String toString() {
        return "\n.ID : " + this.id_claseProfesor + "\n.ID_Estudiante_FK : " +this.fk_profesor + "\n.ID_Clase_FK : " + this.fk_clase; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
