package modelo;

/**
 *
 * @author chris
 */
public class ClaseEstudiante {
    
    private int id_claseEstudiante;
    private int fk_estudiante;
    private int fk_clase;

    public ClaseEstudiante(int id_estudiante, int id_clase) {
       
        this.fk_estudiante = id_estudiante;
        this.fk_clase = id_clase;
    }

    /**
     * @return the id_claseEstudiante
     */
    public int getId_claseEstudiante() {
        return id_claseEstudiante;
    }

    /**
     * @param id_claseEstudiante the id_claseEstudiante to set
     */
    public void setId_claseEstudiante(int id_claseEstudiante) {
        this.id_claseEstudiante = id_claseEstudiante;
    }

    /**
     * @return the fk_estudiante
     */
    public int getFk_estudiante() {
        return fk_estudiante;
    }

    /**
     * @param fk_estudiante the fk_estudiante to set
     */
    public void setFk_estudiante(int fk_estudiante) {
        this.fk_estudiante = fk_estudiante;
    }

    /**
     * @return the fk_clase
     */
    public int getFk_clase() {
        return fk_clase;
    }

    /**
     * @param fk_clase the fk_clase to set
     */
    public void setFk_clase(int fk_clase) {
        this.fk_clase = fk_clase;
    }

    @Override
    public String toString() {
        return ".\nID : " + this.id_claseEstudiante + "\n.ID_Estudiante_FK : " +this.fk_estudiante + "\n.ID_Clase_FK : " + this.fk_clase; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
