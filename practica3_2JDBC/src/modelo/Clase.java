package modelo;

/**
 *
 * @author chris
 */
public class Clase {
    
    private int id_clase;
    private String nombre_clase;
    private String siglo;

    public Clase(String nombre_clase, String siglo) {
       
        this.nombre_clase = nombre_clase;
        this.siglo = siglo;
    }

    /**
     * @return the id_clase
     */
    public int getId_clase() {
        return id_clase;
    }

    /**
     * @param id_clase the id_clase to set
     */
    public void setId_clase(int id_clase) {
        this.id_clase = id_clase;
    }

    /**
     * @return the nombre_clase
     */
    public String getNombre_clase() {
        return nombre_clase;
    }

    /**
     * @param nombre_clase the nombre_clase to set
     */
    public void setNombre_clase(String nombre_clase) {
        this.nombre_clase = nombre_clase;
    }

    /**
     * @return the siglo
     */
    public String getSiglo() {
        return siglo;
    }

    /**
     * @param siglo the siglo to set
     */
    public void setSiglo(String siglo) {
        this.siglo = siglo;
    }

    @Override
    public String toString() {
        return  "\n.ID : " +  this.id_clase  + "\n.Clase : " + this.nombre_clase   + "\n.Siglo : " + this.siglo; //To change body of generated methods, choose Tools | Templates.
    }
    
    

    
    
    
    
    
    
}
