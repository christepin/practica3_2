package modelo;

import java.util.List;

/**
 *
 * @author chris
 */
public interface claseDAO {
    
  public Clase obtenernombre(String nombre_clase);
  public Clase obtenerSiglo(String siglo);
  public List<Clase> obtenerClases();
  public Clase crearClase(String nombreClase, String siglo);
  public boolean eliminarClases(int idClases);
}
