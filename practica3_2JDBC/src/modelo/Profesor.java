
package modelo;

/**
 *
 * @author chris
 */
public class Profesor {
    
    private int id_profesor;
    private String nombre_profesor;
    private String apellido_profesor;
    private String email;

    public Profesor(String nombre_profesor, String apellido_profesor, String email) {
        this.nombre_profesor = nombre_profesor;
        this.apellido_profesor = apellido_profesor;
        this.email = email;
    }

    /**
     * @return the id_profesor
     */
    public int getId_profesor() {
        return id_profesor;
    }

    /**
     * @param id_profesor the id_profesor to set
     */
    public void setId_profesor(int id_profesor) {
        this.id_profesor = id_profesor;
    }

    /**
     * @return the nombre_profesor
     */
    public String getNombre_profesor() {
        return nombre_profesor;
    }

    /**
     * @param nombre_profesor the nombre_profesor to set
     */
    public void setNombre_profesor(String nombre_profesor) {
        this.nombre_profesor = nombre_profesor;
    }

    /**
     * @return the apellido_profesor
     */
    public String getApellido_profesor() {
        return apellido_profesor;
    }

    /**
     * @param apellido_profesor the apellido_profesor to set
     */
    public void setApellido_profesor(String apellido_profesor) {
        this.apellido_profesor = apellido_profesor;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
       @Override
    public String toString(){
        
        return "\n.ID : "+ this.id_profesor +  "\n.Nombre : "+ this.nombre_profesor + "\n.Apellido : " + this.apellido_profesor + "\n.Email : " + this.email ;
    }
    
    
    
}
