/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author chris
 */
public interface intermediaEstudianteClase {
  public ClaseEstudiante obtenerIdEstudiante(String id_estudiante);
  public ClaseEstudiante obtenerIdClase(String id_clase);
  public List<ClaseEstudiante> obtenerIdClasesEstudiantes();
  public ClaseEstudiante crearClaseEstudiante(int id_estudiante, int id_clase);
  public boolean eliminarIdClaseEstudiante(int id_claseEstudiante);
}
