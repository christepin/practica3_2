
package modelo;

import java.util.List;

/**
 *
 * @author chris
 */
public interface profesorDAO {
    
  public Profesor obtenerNombre(String nombre_profesor);
  public Profesor obtenerApellido(String apellido_profesor);
  public Profesor obtenerEmail(String email_profesor);
  public List<Profesor> obtenerProfesores();
  public Profesor crearProfesor(String nombreProfesor, String apellidoProfesor , String emailProfesor);
  public boolean eliminarProfesores(int idProfesor);
}
