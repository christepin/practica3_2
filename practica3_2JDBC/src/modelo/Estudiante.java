
package modelo;

/**
 *
 * @author chris
 */
public class Estudiante {
    
    private int id_estudiante;
    private String nombre_estudiante;
    private String apellido_estudiante;

    public Estudiante(String nombre_estudiante, String apellido_estudiante) {
        
        this.nombre_estudiante = nombre_estudiante;
        this.apellido_estudiante = apellido_estudiante;
    }

    /**
     * @return the id_estudiante
     */
    public int getId_estudiante() {
        return id_estudiante;
    }

    /**
     * @param id_estudiante the id_estudiante to set
     */
    public void setId_estudiante(int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }

    /**
     * @return the nombre_estudiante
     */
    public String getNombre_estudiante() {
        return nombre_estudiante;
    }

    /**
     * @param nombre_estudiante the nombre_estudiante to set
     */
    public void setNombre_estudiante(String nombre_estudiante) {
        this.nombre_estudiante = nombre_estudiante;
    }

    /**
     * @return the apellido_estudiante
     */
    public String getApellido_estudiante() {
        return apellido_estudiante;
    }

    /**
     * @param apellido_estudiante the apellido_estudiante to set
     */
    public void setApellido_estudiante(String apellido_estudiante) {
        this.apellido_estudiante = apellido_estudiante;
    }
    
    @Override
    public String toString(){
        
        return  "\n.ID : "+ this.id_estudiante +  "\n.Nombre : "+ this.nombre_estudiante + "\n.Apellido : " + this.apellido_estudiante ;
    }
    
    
  
    
}
