package modelo;

import java.sql.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author chris
 */
public class  Conexion{
    
    private String nombreBaseDatos;
    private String url;
    private String usuario ;
    private String password ;
    
    private Connection con ;
    private Statement st;
    private ResultSet res;
   
 

    public Conexion(String nombreBaseDatos, String usuario, String password) {
        this.nombreBaseDatos = nombreBaseDatos;
        this.usuario = usuario;
        this.password = password;
        url = "jdbc:mysql://localhost:3306/"+ this.nombreBaseDatos +"?characterEncoding=utf8";
        
        connected();
        
     }
    
    public void connected(){
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, usuario, password);
            
            st = con.createStatement();
            System.out.println("Connected...");
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResultSet consulta(String query){
        
        try {
        
          res = st.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    
    
      
    public void closeConexion(){
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
    
    
    

 


    
}
