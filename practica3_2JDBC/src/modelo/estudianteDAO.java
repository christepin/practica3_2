
package modelo;

import java.util.List;

/**
 *
 * @author chris
 */
public interface estudianteDAO {
    
  public Estudiante obtenerNombre(String nombre_estudiante);
  public Estudiante obtenerApellido(String apellido_estudiante);
  public List<Estudiante> obtenerEstudiantes();
  public Estudiante crearEstudiante(String nombreEstudiante, String apellidoEstudiante);
  public boolean eliminarEstudiante(int idEstudiante);
  
}
