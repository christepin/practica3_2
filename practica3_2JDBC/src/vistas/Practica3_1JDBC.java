
package vistas;

import modelo.Conexion;
import modelo.Clase;
import modelo.ClaseEstudiante;
import modelo.ClaseProfesor;
import modelo.Estudiante;
import modelo.Profesor;

/**
 *
 * @author chris
 */
public class Practica3_1JDBC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
       
       Conexion co = new Conexion("curso", "root", ""); //tiene el password dentro el fichero txt mandado por Teams
   
        System.out.println("Conección OK");
       
       Profesor p = new Profesor("Jack Ray", "Bastos" , "jackR@email");
       
       Estudiante e = new Estudiante("Emilie Rose", "Tello");
       
       Clase cl = new Clase("Estatisticás", "EST");
       
       ClaseEstudiante clE = new ClaseEstudiante(0, 0);
       
       ClaseProfesor clP = new ClaseProfesor(0, 0);
       
       
        System.out.println("\nPROFESOR"+p.toString() +"\n\nESTUDIANTE"+ e.toString() +"\n\nCLASE"+ cl.toString() + "\n\nCLASE_ESTUDIANTE" + clE.toString() + "\n\nCLASE_PROFESOR" + clP.toString());
    
       
     
       
 
        
        co.closeConexion();
        
        
               
    }
    
}
